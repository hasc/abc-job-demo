# abc-job-demo

Deployment configuration of ABC JobServer to a local Kubernetes cluster based on [Skaffold](https://skaffold.dev/docs/).

Essentially the deployment is based on the images. One is the [job-server](https://gitlab.com/hasc/job-server/), a Symfony application configured with the [abc/job-server-bundle](https://github.com/aboutcoders/job-server-bundle). The other is the [job-worker](https://gitlab.com/hasc/job-worker/), a Symfony application configured with the [abc/job-worker-bundle](https://github.com/aboutcoders/job-worker-bundle). Both repositories contain the Dockerfile that is used to build the images deployed to the k8 cluster.

The following services will be deployed:

| Service name           | Image      | Description |
|:-----------------------|:-----------|:------------|
| abc-job-server         | job-server | Provides the HTTP API to manage jobs, cron jobs, and routes |
| abc-job-scheduler      | job-server | Watches cron jobs and calls job-server when cron jobs are due |
| abc-job-reply-consumer | job-server | Processes job replies, consumed as messages from a queue |
| abc-worker*            | job-worker | Processes jobs by name or from a given queue |
| mysql                  | official   | Persistence layer |
| rabbitmq               | official   | Message Broker used for message exchange between server and workers |

## Prerequisites

Ensure the following cli commands are installed on your system and are up to date:

1. [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/)
2. [Skaffold](https://skaffold.dev)
3. [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

## Starting the Demo

1. Create the base project directory

    ```bash
    mkdir -p /path/to/abc-job-server && cd /path/to/abc-job-server
    ```

2. Clone the [JobServer](https://gitlab.com/hasc/job-server/) and [JobWorker](https://gitlab.com/hasc/job-worker/)

    ```bash
    git clone git@gitlab.com:hasc/job-server.git
    ```
    
   ```bash
    git clone git@gitlab.com:hasc/job-worker.git
    ```

3. Clone this repo

    ```bash
    git clone git@gitlab.com:hasc/abc-job-demo.git
    ```

4. Ensure the Kubernetes context is set

    In case you're using Docker for Desktop you might do
    
    ```bash
   kubectl config use-context docker-desktop 
   ```
   
5. Deploy services

    ```bash
   skaffold dev --port-forward
   ```
   
6. Access the Job-Api documentation

    Have a look at the port the service `abc-job-server` is forwarded to, should be `4503`

    <http://localhost:4503/doc>


## Service Endpoints

* Job Server API
    * OpenApi reference <http://localhost:4503/doc>
    * Job API <http://localhost:8080/job>
    * CronJob API <http://localhost:8080/cronjob>
    * Route API <http://localhost:8080/cronjob>
* RabbitMQ Management Dashboard: <http://localhost:15672>
* RabbitMQ: <http://localhost:5672>
* MySQL <http://localhost:3306>

Please note that the actual ports might differ on your host. Have a look at the console output of Skaffold to see the actually exposed ports.

## Working with the demo

There are three test jobs available with the name `job_A`, `job_B`, and `job_C`. You can use it to process jobs, sequences and batches. The jobs behave in a certain way depending on the json input provided with the job.

* `sendOutput`: Some output that will be returned while processing (should be used in combination wil `sleep`)
* `exception`: An exception message that will be thrown when the job is processed
* `sleep`: number of seconds to sleep
* `output`: some output returned when the job completes
* `fail`: boolean, whether to terminate the job with status "failed" or "complete"

### Examples

Send a POST request to <http://localhost:4503/job> with once of the following request bodies

1. Create a job will be created that sleeps for 1 second.

	```json
	{
		"type":"Job",
		"name":"job_A",
		"input":"{\"sleep\":1}"
	}
	```

2. Create a Sequence (processed sequentially) consisting of two jobs.

	```json
	{
		"type": "Sequence",
		"children": [
			{
				"type": "Job",
				"name": "job_A"
			},
			{
				"type": "Job",
				"name": "job_B"
			}
		]
	}
	```

3. Create a Batch (processed in parallel) consisting of two jobs.

	```json
	{
		"type": "Sequence",
		"children": [
			{
				"type": "Job",
				"name": "job_A"
			},
			{
				"type": "Job",
				"name": "job_B"
			}
		]
	}
	```
